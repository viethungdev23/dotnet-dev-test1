## bài tập
  hoàn thành hàm ```SortArrayDesc```
  với tham số đầu vào hàm là mảng số nguyên x
  kết quả hàm sau khi thực thi là mảng số nguyên mới đã sắp xếp theo thứ tự giảm dần
  ví dụ: x = [4, 2, 3, 4]
  => kế quả hàm sẽ là trả về [4, 4, 3, 2]

## lưu ý: khi nộp bài source code phải luôn luôn build thành công.

## kiểm tra kết quả của mình bằng cách chạy unit test
	trên thanh menu của Visual studio -> Test -> Test Explorer -> chạy test để kiểm tra kết quả
